package com.example.karol.practica4;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,FrgUn.OnFragmentInteractionListener,FrgDos.OnFragmentInteractionListener{
    Button botonFragUno , botonFraDos ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botonFragUno = (Button)findViewById(R.id.btnFrgUno);
        botonFraDos =(Button)findViewById(R.id.btnFrgDos);
        botonFragUno.setOnClickListener(this);
        botonFraDos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrgUno:
            FrgUn fragmentoUno = new FrgUn ();
                FragmentTransaction transactionUn = getSupportFragmentManager().beginTransaction();
                transactionUn.replace(R.id.contenedor,fragmentoUno) ;
                transactionUn.commit();
                break;
            case R.id.btnFrgDos :
                FrgDos fragmentoDos = new FrgDos ();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor,fragmentoDos) ;
                transactionDos.commit();
                break;



        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


}
